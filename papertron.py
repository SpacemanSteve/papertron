#!/usr/bin/env python

import docker
import logging
import yaml
import urllib.request

from jinja2 import Template
from time import sleep
import webbrowser

logger = logging.getLogger("")
logger.setLevel(logging.DEBUG)


dockerfile_contents = """
from jupyter/base-notebook

{% for dataset in data %}
ADD {{ dataset }} data/
{% endfor %}

ADD notebooks notebooks

{% for line in build_script %}
RUN {{ line }}
{% endfor %}
"""


def build_image(tag, dockerfile, client):
    client.images.build(path=".", tag=tag)



def build_dockerfile(configuration):
    template = Template(dockerfile_contents)
    remote_datasets = [data['uri'] for data in config.data]
    datasets = []
    for idx, dataset in enumerate(remote_datasets):
        urllib.request.urlretrieve (dataset, f"dataset_{idx}")
        datasets.append(f"dataset_{idx}")
    build_script = configuration.build_script
    contents = template.render(data=datasets, build_script=build_script)
    with open("Dockerfile", "w") as dockerfile:
        dockerfile.write(contents)



class PaperConfig:
    def __init__(self, paper, build_script, data, notebooks) :
        self.paper = paper
        self.build_script = build_script
        self.data = data
        self.notebooks = notebooks


def read_configuration():
    with open("papertron.yaml", "r") as config_file:
        config = yaml.load(config_file)

    return PaperConfig(config['paper'], config['build'], config['data'], config['notebooks'])

def get_jupyter_uri(container):
    """
    run command in docker container to obtain uri for jupyter notebook

    sample ExecRsult object on osx (same on other platforms?):
    ExecResult(exit_code=0, output=b'Currently running servers:\nhttp://localhost:8888/?token=68384fe26c45d0c97c4cfbd5ea4530c500701ad235a8531e :: /home/jovyan\n')
    """
    notebooks = container.exec_run('jupyter notebook list')
    # simple parse of notebooks to get uri with token of first notebook
    n1 = notebooks.output.decode().split('\n')[1]
    u = n1[0:].split(' ')[0]
    parts = u.split('?')
    # get the name of the notebook /home/jovyan is available from the above list command
    name = container.exec_run('ls /home/jovyan/notebooks')
    n1 = name.output.decode().strip()
    u = parts[0] + 'tree/notebooks/' + n1 + '?' + parts[1]
    return u
    
if __name__ == "__main__":
    print("reading config")
    config = read_configuration()
    
    print("building dockerfile")
    dockerfile = build_dockerfile(config)

    print("building image")
    client = docker.from_env()
    tag = config.paper[0]['name']
    build_image(tag, dockerfile, client)

    print("running container")
    container = client.containers.run(tag, detach=True, name=tag, ports={8888:8888})

    # give container time to start and then open browser to notebook
    print("launching browser to notebook in 5 seconds...")
    sleep(5)
    uri = get_jupyter_uri(container)
    webbrowser.open(uri, new=1)
    
