
The project requires python 3.6 at this point. You also need Docker.

Install the dependencies with

```
pip install -r requirements.txt
```

Then run

```
./papertron.py
```

This will create a `Dockerfile` based on the info in `papertron.yaml`.

It will dowload datasets and unzip/untar them into the image if necessary.

It will also install the software as instructed in the `papertron.yaml`.

And it will copy any notebooks in the `notebooks` folder.

Then it starts the container and launches a browser to the jupyter notebook.

Eventually, the idea is to get the metadata currently in `papertron.yaml` from the
ADS query, using the bibcode as a handle.

